package com.mjk.mowitnow;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TondeuseTest {

	@Test
	public void negativeX() throws IllegalParameterException {
		new Tondeuse(1, -1, 0, "N");
	}

	@Test
	public void negativeY() throws IllegalParameterException {
		new Tondeuse(1, 0, -1, "N");
	}

	@Test
	public void impossibleDirection() throws IllegalParameterException {
		new Tondeuse(1, 0, 0, "M");
	}

	@Test
	public void turn() throws IllegalParameterException {
		Tondeuse tondeuse = new Tondeuse(1, 0, 0, "N");

		tondeuse.move(Control.GAUCHE, 5, 5);
		assertEquals(Direction.WEST, tondeuse.getDirection());
		tondeuse.move(Control.GAUCHE, 5, 5);
		assertEquals(Direction.SOUTH, tondeuse.getDirection());
		tondeuse.move(Control.GAUCHE, 5, 5);
		assertEquals(Direction.EAST, tondeuse.getDirection());
		tondeuse.move(Control.GAUCHE, 5, 5);
		assertEquals(Direction.NORTH, tondeuse.getDirection());

		tondeuse.move(Control.DROITE, 5, 5);
		assertEquals(Direction.EAST, tondeuse.getDirection());
		tondeuse.move(Control.DROITE, 5, 5);
		assertEquals(Direction.SOUTH, tondeuse.getDirection());
		tondeuse.move(Control.DROITE, 5, 5);
		assertEquals(Direction.WEST, tondeuse.getDirection());
		tondeuse.move(Control.DROITE, 5, 5);
		assertEquals(Direction.NORTH, tondeuse.getDirection());
	}

	@Test
	public void forward() throws IllegalParameterException {
		Tondeuse tondeuse;

		tondeuse = new Tondeuse(1, 0, 0, "N");
		tondeuse.move(Control.AVANT, 1, 1);
		assertEquals(Direction.NORTH, tondeuse.getDirection());
		assertEquals(new XYCooordonnees(0, 1), tondeuse.getCoordonnees());

		tondeuse = new Tondeuse(1, 0, 0, "E");
		tondeuse.move(Control.AVANT, 1, 1);
		assertEquals(Direction.EAST, tondeuse.getDirection());
		assertEquals(new XYCooordonnees(1, 0), tondeuse.getCoordonnees());

		tondeuse = new Tondeuse(1, 1, 1, "S");
		tondeuse.move(Control.AVANT, 1, 1);
		assertEquals(Direction.SOUTH, tondeuse.getDirection());
		assertEquals(new XYCooordonnees(1, 0), tondeuse.getCoordonnees());

		tondeuse = new Tondeuse(1, 1, 1, "W");
		tondeuse.move(Control.AVANT, 1, 1);
		assertEquals(Direction.WEST, tondeuse.getDirection());
		assertEquals(new XYCooordonnees(0, 1), tondeuse.getCoordonnees());
	}

	@Test
	public void forwardBelowZero() throws IllegalParameterException {
		Tondeuse tondeuse = new Tondeuse(1, 0, 0, "W");
		tondeuse.move(Control.AVANT, 1, 1);
		assertEquals(Direction.WEST, tondeuse.getDirection());
		assertEquals(new XYCooordonnees(0, 0), tondeuse.getCoordonnees());
	}

	@Test
	public void forwardAboveMaximum() throws IllegalParameterException {
		Tondeuse tondeuse = new Tondeuse(1, 4, 4, "E");
		tondeuse.move(Control.AVANT, 4, 4);
		assertEquals(Direction.EAST, tondeuse.getDirection());
		assertEquals(new XYCooordonnees(4, 4), tondeuse.getCoordonnees());
	}

	@Test
	public void mowerToString() throws IllegalParameterException {
		Tondeuse tondeuse = new Tondeuse(1, 1, 2, "N");
		assertEquals("1 2 N", tondeuse.toString());

	}


}
