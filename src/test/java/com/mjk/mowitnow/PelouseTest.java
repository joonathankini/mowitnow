package com.mjk.mowitnow;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

class PelouseTest {

	@Test
	public void negativeX() throws IllegalParameterException {
		new Pelouse(-1, 1);
	}

	@Test
	public void negativeY() throws IllegalParameterException {
		new Pelouse(1, -1);
	}

	@Test
	public void tondeuseOutOfFieldX() throws IllegalParameterException {
		new Pelouse(1, 1).addTondeuse(new Tondeuse(1, 2, 1, "N"));
	}

	@Test
	public void tondeuseOutOfFieldY() throws IllegalParameterException {
		new Pelouse(1, 1).addTondeuse(new Tondeuse(1, 1, 2, "N"));
	}

	@Test
	public void addTondeuseNoException() throws IllegalParameterException {
		Pelouse pelouse = new Pelouse(5, 5);
		Tondeuse tondeuse1 = new Tondeuse(1, 1, 1, "N");
		Tondeuse tondeuse2 = new Tondeuse(2, 2, 2, "S");

		pelouse.addTondeuse(tondeuse1);
		pelouse.addTondeuse(tondeuse2);

		List<Tondeuse> tondeuses = pelouse.getTondeuses();

		assertEquals(tondeuse1, tondeuses.get(0));
		assertEquals(tondeuse2, tondeuses.get(1));
	}

}
