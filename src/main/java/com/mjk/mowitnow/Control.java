package com.mjk.mowitnow;
/**
 * Une enumeration representant un control à envoyer à la tondeuse
 * 
 * @author jonathan
 *
 */
public enum Control {
	
	AVANT("A"), 
	GAUCHE("G"), 
	DROITE("D");

	private String representation;

	private Control(String representation) {
	    this.representation = representation;
	  }

	/**
	 * Trouver un control à partir d'une representation
	 * 
	 * @param representation: la representation du control à trouver 
	 * @return un control à partir de la representation
	 * @throws IllegalParameterException : execption si la represenation n'existe pas
	 *      
	 */
	public static Control TrouverParRepresentation(String representation) throws IllegalParameterException {
		Control[] controls = Control.values();
		for (Control control : controls) {
			if (control.representation.equals(representation)) {
				return control;
			}
		}
		throw new IllegalParameterException(representation, "une bonne representation de control");
	}

	@Override
	public String toString() {
		return this.representation;
	}

}
