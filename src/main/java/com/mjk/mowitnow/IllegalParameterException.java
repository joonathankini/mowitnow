package com.mjk.mowitnow;

public class IllegalParameterException extends Exception{
	private static final long serialVersionUID = 2978961698664784338L;

	public IllegalParameterException(Object entrée, String message) {
		super(IllegalParameterException.getMessage(entrée, message));
	}

	private static String getMessage(Object entrée, String message) {
		StringBuilder builder = new StringBuilder();

		builder.append("Illegal parameter : message ")
			   .append(message == null ? "null" : message).append(" ; Valeur Saisi:  ")
			   .append(entrée == null ? "null" : entrée);

		return builder.toString();
	}

}
