package com.mjk.mowitnow;

import java.util.regex.Pattern;

public class InputParserException extends Exception {

	  private static final long serialVersionUID = -5086622324557612970L;

	  public InputParserException(String input, Pattern pattern) {
	    super(InputParserException.getMessage(input, pattern));
	  }

	  private static String getMessage(String input, Pattern pattern) {
	    StringBuilder builder = new StringBuilder();

	    builder.append("Une erreur s'est produite pendant la validation de l'entrée : ")
	           .append("Entrée attendue doit correspondre au pattern ")
	           .append(pattern == null ? "null" : pattern)
	           .append(" ; Saisi: ")
	           .append(input == null ? "null" : input)
	           .append(" ne respecte pas au pattern.");

	    return builder.toString();
	  }

	}
