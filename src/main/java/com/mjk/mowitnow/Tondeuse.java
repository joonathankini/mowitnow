package com.mjk.mowitnow;

/**
 * Represente une tondeuse
 * 
 * Une tondeuse a une direction, des coordonnees (x,y) et peut se deplacer grace aux controls qui lui sont passés
 * 
 * @author jonathan
 *
 */
public class Tondeuse {
	
	private int id;
	private XYCooordonnees coordonnees;
	private Direction direction;

	public Tondeuse(int id, int x, int y, String directionRepresentation) throws IllegalParameterException {
	    if (x < 0) {
	      throw new IllegalParameterException(x, "a positive Integer");
	    } else if (y < 0) {
	      throw new IllegalParameterException(y, "a positive Integer");
	    }
	    this.id = id;
	    this.coordonnees = new XYCooordonnees(x, y);
	    this.direction = Direction.TrouverParRepresentation(directionRepresentation);
	  }

	public Tondeuse(Tondeuse tondeuse) {
	    this.setId(tondeuse.getId());
	    this.coordonnees = new XYCooordonnees(tondeuse.getCoordonnees());
	    this.direction = tondeuse.getDirection();
	  }

	/**
	 * Faire deplacer la tondeuse sur la pelouse en fonction des controls qui lui sont passés
	 * 
	 * @param control: pour aller tout droit ou se tourner
	 * @param maxX: a largeur de la pelouse
	 * @param maxY: la longeur de la pelouse
	 * @throws IllegalParameterException
	 */
	public void move(Control control, int maxX, int maxY) throws IllegalParameterException {
		if (Control.AVANT.equals(control)) {
			coordonnees.setX(Math.max(Math.min(coordonnees.getX() + direction.getX(), maxX), 0));
			coordonnees.setY(Math.max(Math.min(coordonnees.getY() + direction.getY(), maxY), 0));
		} else {
			direction = direction.turn(control);
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public XYCooordonnees getCoordonnees() {
		return coordonnees;
	}

	public Direction getDirection() {
		return direction;
	}

	@Override
	public String toString() {
		return coordonnees + " " + direction;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coordonnees == null) ? 0 : coordonnees.hashCode());
		result = prime * result + ((direction == null) ? 0 : direction.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tondeuse other = (Tondeuse) obj;
		if (coordonnees == null) {
			if (other.coordonnees != null)
				return false;
		} else if (!coordonnees.equals(other.coordonnees))
			return false;
		if (direction != other.direction)
			return false;
		if (id != other.id)
			return false;
		return true;
	}

}
