package com.mjk.mowitnow;

import java.util.Scanner;

public class App {
	
	public static void main(String[] args) throws InputParserException, IllegalParameterException {

		String inputString;

		/*
		 * inputString = "5 5\n" + "1 2 N\n" + "GAGAGAGAA\n" + "3 3 E\n" + "AADAADADDA";
		 */

		System.out.println("DEBUT");

		Scanner sc = new Scanner(System.in);
		System.out.println("Saisir une entrée: ");
		inputString = sc.nextLine();

		System.out.println("INFOROMATIONS EN ENTREE: ");

		Solution solution = new Solution();
		System.out.println(Solution.getSolution(inputString));

		System.out.println("FIN");
	}

}
