package com.mjk.mowitnow;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Represente une pelouse
 * 
 * Une pelouse a une liste de tondeuse, une certaine longeur et largeur
 * 
 * @author jonathan
 *
 */
public class Pelouse {
	
	private List<Tondeuse> tondeuses;
	private int width;
	private int height;

	public Pelouse(int width, int height) throws IllegalParameterException {
	    if (width < 0) {
	      throw new IllegalParameterException(width, " un nombre positif");
	    } else if (height < 0) {
	      throw new IllegalParameterException(height, " un nombre positif");
	    }

	    this.tondeuses = new ArrayList<Tondeuse>();
	    this.width = width;
	    this.height = height;
	  }

	/**
	 * Ajoute une tondeuse a fin de la liste des tondeuses
	 * 
	 * @param tondeuse: la tondeuse à ajouter
	 * @throws IllegalParameterException: si les coordonnees de la tondeuse sont impossibles
	 *                       
	 */
	public void addTondeuse(Tondeuse tondeuse) throws IllegalParameterException {
		int x = tondeuse.getCoordonnees().getX();
		int y = tondeuse.getCoordonnees().getY();

		if (x > width) {
			throw new IllegalParameterException(x, "une valeur inferieur à la largeur de la pelouse");
		}

		if (y > height) {
			throw new IllegalParameterException(y, " une valeur inferieiur a la longeur de la pelouse");
		}

		this.tondeuses.add(tondeuse);
	}

	/**
	 * Faire se deplacer une tondeuse de la liste des tondeuses
	 * 
	 * @param control: le control à appliquer 
	 * @throws IllegalParameterException
	 */
	public void moveLastTondeuse(Control control) throws IllegalParameterException {
		tondeuses.get(tondeuses.size() - 1).move(control, width, height);
	}

	/**
	 * Retoure une copie des tondeuses sur la pelouse
	 * 
	 * @return une copie des tondeuses
	 * @throws IllegalParameterException
	 */
	/*public List<Tondeuse> getCopyOTondeuse() throws IllegalParameterException {
		List<Tondeuse> tondeuses = new ArrayList<Tondeuse>();
		for (Tondeuse tondeuse : this.tondeuses) {
			tondeuses.add(new Tondeuse(tondeuse));
		}
		return tondeuses;
	}*/

	public List<Tondeuse> getTondeuses() {
		return tondeuses;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	/*@Override
	public String toString() {
		return StringUtils.join(tondeuses, "\n");
	}*/

}
