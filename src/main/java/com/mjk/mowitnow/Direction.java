package com.mjk.mowitnow;


/**
 * Une enumeration representant une Direction
 * 
 * Une direction connait la direction à sa gauche et sa droite
 * 
 * @author jonathan
 *
 */
public enum Direction {
	
	NORTH("N", "WEST", "EAST", 0, 1), 
	EAST("E", "NORTH", "SOUTH", 1, 0), 
	SOUTH("S", "EAST", "WEST", 0, -1),
	WEST("W", "SOUTH", "NORTH", -1, 0);

	private String representation;
	private String directionLeft;
	private String directionRight;
	private int x;
	private int y;

	private Direction(String representation, String directionLeft, String directionRight, int x, int y) {
		this.representation = representation;
		this.directionLeft = directionLeft;
		this.directionRight = directionRight;
		this.x = x;
		this.y = y;
	}

	/**
	 * Trouver une direction à partir d'une representation
	 * 
	 * @param representation : la representation de la direction à trouver
	 * @return une direction à partir de la representation
	 * @throws IllegalParameterException: Exception si aucune direction n'existe pour cette represenation                               representation
	 */
	public static Direction TrouverParRepresentation(String representation) throws IllegalParameterException {
		Direction[] directions = Direction.values();
		for (Direction direction : directions) {
			if (direction.representation.equals(representation)) {
				return direction;
			}
		}
		throw new IllegalParameterException(representation, " une bonne representation de direction");
	}

	/**
	 * Retourne une nouvelle direction en tournant à gauche ou à droite
	 * 
	 * @param control: un control qui specifie de tourner à gauche ou à droite
	 * @return une nouvelle direction
	 * @throws IllegalParameterException: exception si le control n'existe pas
	 */
	public Direction turn(Control control) throws IllegalParameterException {
		if (Control.GAUCHE.equals(control)) {
			return Direction.valueOf(this.directionLeft);
		} else if (Control.DROITE.equals(control)) {
			return Direction.valueOf(this.directionRight);
		} else {
			throw new IllegalParameterException(control, " un control pour aller à gauche ou à droite");
		}
	}

	public String getRepresentation() {
		return representation;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return this.representation;
	}

}
