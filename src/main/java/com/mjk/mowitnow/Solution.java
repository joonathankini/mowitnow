package com.mjk.mowitnow;

import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.tuple.Pair;

/**
 * @author jonathan
 *
 */

public class Solution {
		
	public Solution () {}
	
	public static String getSolution(String inputString) throws InputParserException, IllegalParameterException {
		Scanner inputScanner = new Scanner(inputString);
		UneEntree uneEntree = ParserEntree.ParserEntreee(inputScanner);
		Pelouse pelouse = uneEntree.getPelouse();
		System.out.println(" Pelouse :"+pelouse.getWidth() + " " + pelouse.getHeight());
		int i = 0;
		for (Pair<Tondeuse, List<Control>> tondeuseAndControls : uneEntree.getTondeuseAndControls()) {
			Tondeuse tondeuse = tondeuseAndControls.getLeft(); 
			System.out.println(" tondeuse " + i + " :\n " + tondeuse.toString());
			i++;
			List<Control> controls = tondeuseAndControls.getRight();
			String res = " ";
			for (Control c: controls) {
				res += c.toString();
			}
			System.out.println(res);
		}
		
		return addTondeuses(uneEntree,pelouse);
	}
	

	private static void dispatchControls(List<Control> controls, Pelouse pelouse) throws IllegalParameterException {
		for (Control control : controls) {
			pelouse.moveLastTondeuse(control);
		}
	}

	private static String addTondeuses(UneEntree uneEntree, Pelouse pelouse) throws IllegalParameterException {
		int i = 0;
		String res = "\nSOLUTIONS :";
		for (Pair<Tondeuse, List<Control>> tondeuseAndControls : uneEntree.getTondeuseAndControls()) {
			Tondeuse tondeuse = tondeuseAndControls.getLeft();
			pelouse.addTondeuse(tondeuse);
			List<Control> controls = tondeuseAndControls.getRight();
			dispatchControls(controls, pelouse);

			res+= "\n SOLUTION "+i+" :" + tondeuse.toString();
			i++;
		}
		return res;

	}
	
	public static void main(String [] args) throws InputParserException, IllegalParameterException {
		
		String inputString;
		
		/*
		 inputString = "5 5\n" + 
				"1 2 N\n" + 
				"GAGAGAGAA\n" + 
				"3 3 E\n" + 
				"AADAADADDA";
		*/
		
		System.out.println("DEBUT");
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Saisir une entrée: ");
		inputString = sc.nextLine();
		
		System.out.println("INFOROMATIONS EN ENTREE: ");
		
		Solution solution = new Solution();
		System.out.println(Solution.getSolution(inputString));	
		
		System.out.println("FIN");
	}
	
}
