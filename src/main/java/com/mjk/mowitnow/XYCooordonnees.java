package com.mjk.mowitnow;

/**
 * Une classe representant un coordonnees (x,y)
 * 
 * @author jonathan
 *
 */
public class XYCooordonnees {
	
	private int x;
	private int y;

	public XYCooordonnees(int x, int y) {
	    this.x = x;
	    this.y = y;
	  }

	public XYCooordonnees(XYCooordonnees coordinate) {
	    this.x = coordinate.getX();
	    this.y = coordinate.getY();
	  }

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return x + " " + y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XYCooordonnees other = (XYCooordonnees) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	  
}
