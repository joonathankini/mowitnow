package com.mjk.mowitnow;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.apache.commons.lang3.tuple.Pair;


/**
 * Une classe qui parse les entrées
 * 
 * @author jonathan
 *
 */

public class ParserEntree {

	private static final Pattern REGEX_DIRECTION = Pattern.compile("[NESW]");
	private static final Pattern REGEX_COONTROLS = Pattern.compile("[AGD]+");
	private static final Pattern REGEX_INTEGER = Pattern.compile("[0-9]+");

	private ParserEntree() {
	}

	/**
	 * Parser une entrée
	 * 
	 * @param input : la donnée en entréé
	 * @return la pelouse et une liste de tondeuses avec les controls à appliqués
	 * 
	 * @throws InputParserException
	 * @throws IllegalParameterException
	 */
	public static UneEntree ParserEntreee(Scanner input) throws InputParserException, IllegalParameterException {

		// La pelouse
		int width = Integer.valueOf(scanNext(input, REGEX_INTEGER));
		int height = Integer.valueOf(scanNext(input, REGEX_INTEGER));
		Pelouse pelouse = new Pelouse(width, height);

		List<Pair<Tondeuse, List<Control>>> tondeuseAndControls = new ArrayList<Pair<Tondeuse, List<Control>>>();
		// itère dans la liste des tondeuses et de leurs controls
		int id = 1;
		while (input.hasNext()) {
			// Les Tondeuses
			int x = Integer.valueOf(scanNext(input, REGEX_INTEGER));
			int y = Integer.valueOf(scanNext(input, REGEX_INTEGER));
			String directionRepresentation = scanNext(input, REGEX_DIRECTION);
			Tondeuse tondeuse = new Tondeuse(id, x, y, directionRepresentation);
			id++;
			// Les controls
			List<Control> controls = new ArrayList<Control>();
			if (input.hasNext(REGEX_COONTROLS)) {
				String controlRepresentations = scanNext(input, REGEX_COONTROLS);
				for (int i = 0; i < controlRepresentations.length(); i++) {
					String controlRepresentation = Character.toString(controlRepresentations.charAt(i));
					controls.add(Control.TrouverParRepresentation(controlRepresentation));
				}
			}
			tondeuseAndControls.add(Pair.of(tondeuse, controls));
		}

		return new UneEntree(pelouse, tondeuseAndControls);
	}

	/**
	 * Returns l'élément suivant d'un scanner s'il correspond au pattern
	 * 
	 * Throws InputParserException sinon
	 * 
	 * @param scanner: le scanner
	 * @param pattern: le pattern
	 * @return l'element suivant du scanner
	 * @throws InputParserException
	 */
	private static String scanNext(Scanner scanner, Pattern pattern) throws InputParserException {
		if (scanner.hasNext(pattern)) {
			return scanner.next();
		} else {
			throw new InputParserException(scanner.hasNext() ? scanner.next() : "Rien", pattern);
		}
	}

}
