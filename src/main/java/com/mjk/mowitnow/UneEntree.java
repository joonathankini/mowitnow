package com.mjk.mowitnow;

import java.util.List;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Une classe qui contient une pelouse et les tondeuses avec leurs controls
 * 
 * @author jonathan
 *
 */
public class UneEntree {
	
	private Pelouse pelouse;
	private List<Pair<Tondeuse, List<Control>>> tondeuseAndControls;

	public UneEntree(Pelouse pelouse, List<Pair<Tondeuse, List<Control>>> tondeuseAndControls) {
	    this.pelouse = pelouse;
	    this.tondeuseAndControls = tondeuseAndControls;
	  }

	public Pelouse getPelouse() {
		return pelouse;
	}

	public List<Pair<Tondeuse, List<Control>>> getTondeuseAndControls() {
		return tondeuseAndControls;
	}

}
